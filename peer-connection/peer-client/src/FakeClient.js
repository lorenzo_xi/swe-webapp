import { useRef } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { Button } from "react-bootstrap";

import io from "socket.io-client";

const _usr = {
  id:3,
  name:"lorenzo",
  surname:"peri",
  email:"email",
  number:"123",
  isCustomer:true,
};

const _to = {
  id:1,
  name:"bob",
  surname:"bob",
  email:"email",
  number:"123",
  isCustomer:false,
};

//mi connetto al server socket
const socket = io.connect("http://localhost:3000/", { query: { ..._usr} });
//const socket = io.connect("http://localhost:3050/");

function FakeClient() {
  const clientRef = useRef();

  const [peerConnection, setPeerConnection] = useState();
  const [RTCDataChannel, setRTCDataChannel] = useState();
  const [stream, setStream] = useState();

  const servers = {
    iceServers: [
      {
        urls: [
          "stun:stun1.l.google.com:19302",
          "stun:stun2.l.google.com:19302",
        ],
      },
    ],
  };
  const onOpenHandler = () => {
    console.log("canale aperto");
    sendData();
    console.log(peerConnection);
  };

  const onCloseHandler = () => console.log("closing");
  const onErrorHandler = () => console.log("error");
  const onMessageHandler = () => console.log("message");
  const onBufferedAmountLowHandler = () => console.log("bufferedamountlow");

  //  Creo connessione P2P
  const sendData = () => {
    const first = {
      type: "config",
      data: {
        mcmt: new Uint16Array([
          0x01, 0x03, 0x0a, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
          0xff, 0xff, 0x15, 0x35,
        ]),
        format: 0,
      },
    };

    const second = {
      type: "data",
      data: {
        states: new Uint16Array([
          0x01, 0x03, 0x10, 0x00, 0x08, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe9, 0x9c,
        ]),
        alarms: new Uint16Array([
          0x01, 0x03, 0x10, 0x80, 0x20, 0x00, 0x01, 0x00, 0x00, 0x15, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xb6, 0xe5,
        ]),
        measures: new Uint16Array([
          0x01, 0x03, 0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0xf1, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xa0, 0x0f, 0xa0,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x54, 0x62,
        ]),
      },
    };

    RTCDataChannel.send(JSON.stringify(first));
    RTCDataChannel.send(JSON.stringify(second));
    console.log("inviati i dati");
  };

  const startCall = () => {
    setPeerConnection(new RTCPeerConnection(servers));

    const obj = {
      from: _usr,
      // ** Questo probabilmente non serve per la mobile app
      to: _to,
    };

    socket.emit("start-call", obj);
  };

  useEffect(() => {
    if (peerConnection) {
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: true })
        .then((stream) => {
          clientRef.current.srcObject = stream;
          setStream(stream);
          for (const track of stream.getTracks()) {
            console.log("adding track: ", track);
            peerConnection.addTrack(track, stream);
          }
        });

      setRTCDataChannel(peerConnection.createDataChannel("data-channel"));

      peerConnection.addEventListener("track", (event) => {
        console.log("event-track", event);
        document.getElementById("client-video").srcObject = event.streams[0];
      });

      peerConnection.addEventListener("icecandidate", (event) => {
        console.log("client-event-listener-icecandidate", event);
        if (event.candidate) {
          const obj = {
            from: _usr,
            to: _to,
            candidate: event.candidate,
          };
          socket.emit("client-ice-candidate", obj);
        }
      });

      socket.on("accept-call", async (data) => {
        console.log("accept-call", data);

        const offer = await peerConnection.createOffer({
          offerToReceiveAudio: true,
          offerToReceiveVideo: true,
        });
        await peerConnection.setLocalDescription(offer);

        const obj = {
          from: _usr,
          to: _to,
          offer: offer,
        };
        console.log("client-offer", obj);

        socket.emit("client-offer", obj);
      });

      socket.on("technician-offer", async (data) => {
        console.log("technician-offer", data);
        if (data.answer) await peerConnection.setRemoteDescription(data.answer);
      });

      socket.on("technician-ice-candidate", (data) => {
        console.log("technician-ice-candidate", data);

        peerConnection.addIceCandidate(data.candidate);

        console.log(peerConnection.connectionState);
      });
    }
  }, [peerConnection]);

  useEffect(() => {
    if (RTCDataChannel) {
      RTCDataChannel.onopen = onOpenHandler;
      RTCDataChannel.onclose = onCloseHandler;
      RTCDataChannel.onmessage = onMessageHandler;
      RTCDataChannel.onerror = onErrorHandler;
      RTCDataChannel.onbufferedamountlow = onBufferedAmountLowHandler;
    }
  }, [RTCDataChannel]);

  return (
    <>
      <Button
        onClick={() => {
          startCall();
        }}
      >
        Start Call
      </Button>
      <div>
      <video
        className="border"
        playsInline
        autoPlay
        controls
        id="technician-video"
        ref={clientRef}
        width="300px"
      />
      <video
        className="border"
        playsInline
        autoPlay
        id="client-video"
        width="300px"
      />
      </div>
      <Button
        variant="secondary"
        className="mx-3"
        onClick={() => {
          stream.getVideoTracks()[0].enabled =
            !stream.getVideoTracks()[0].enabled;
        }}
      >
        Disable Video
      </Button>
      <Button
        className="mx-3"
        variant="secondary"
        onClick={() => {
          stream.getAudioTracks()[0].enabled =
            !stream.getAudioTracks()[0].enabled;
        }}
      >
        Mute
      </Button>
      <Button className="mx-3" variant="warning" onClick={() => sendData()}>
        Send Data
      </Button>
    </>
  );
}

export default FakeClient;
