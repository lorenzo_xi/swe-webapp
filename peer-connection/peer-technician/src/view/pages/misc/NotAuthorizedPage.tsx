import { Container, Row } from "react-bootstrap";
import vecio from "src/img/vecio.jpg";

const NotAuthorizedPage: React.FC = () => {
  return (
    <Container className="d-flex flex-column justify-content-center">
      <Row>
        <img src={vecio} width="800" height="600" />
      </Row>
      <Row className="text-center">
        <h1>Vecio non sei autorizzato. Fuori di qua!</h1>
      </Row>
    </Container>
  );
};

export default NotAuthorizedPage;
