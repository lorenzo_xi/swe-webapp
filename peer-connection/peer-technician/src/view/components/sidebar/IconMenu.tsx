import { Home, User } from "react-feather";
import { useNavigate } from "react-router-dom";

const IconMenu: React.FC = () => {
  const navigate = useNavigate()

  const homeHandler = () => navigate("/dashboard")

  const technicianDetailsHandler = () => navigate("/technician-details")

  return (
    <div className="d-flex justify-content-evenly">
      <Home size={40} className="border rounded p-2" color="white" role="button" onClick={() => homeHandler()} />
      <User size={40} className="border rounded p-2" color="white" role="button" onClick={() => technicianDetailsHandler()} />
    </div>
  );
};

export default IconMenu;
