import Response from "src/shared/class/response";

describe("Test suite for Response", () => {
  it("should construct an Response", () => {
    const response: Response = new Response(true);
    expect(response).toBeDefined();
    expect(typeof response.getSuccess).toBe("function");
    expect(typeof response.getData).toBe("function");
    expect(response).toBeInstanceOf(Response);
  });

  it("should return the Response success field", () => {
    const response: Response = new Response(true);
    expect(response.getSuccess()).toBe(true);
  });

  it("should return the LoginInfo data field (type object)", () => {
    const response: Response = new Response(true, { msg: "msg" }, "error");
    expect(response.getData()).toEqual({ msg: "msg" });
    expect(response.getError()).toBe("error");
  });

  it("should return the Response null data and error field", () => {
    const response: Response = new Response(true);
    expect(response.getData()).toBe(null);
    expect(response.getData()).toBe(null);
  });

  it("should set the Response status", () => {
    const response: Response = new Response(true);
    response.setStatus(201);
    expect(response.getStatus()).toBe(201);
  });
});
