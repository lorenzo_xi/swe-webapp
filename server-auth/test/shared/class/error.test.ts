import AuthError from "src/shared/class/error";

describe("Test suite for AuthError", () => {
  it("should construct an AuthError (no.params)", () => {
    const err: AuthError = new AuthError();
    expect(err).toBeDefined();
    expect(typeof err.type).toBe("function");
    expect(typeof err.msg).toBe("function");
    expect(err).toBeInstanceOf(AuthError);
  });

  it("should construct an AuthError (passed params)", () => {
    const err: AuthError = new AuthError(1, "msg");
    expect(typeof err.type).toBe("function");
    expect(typeof err.msg).toBe("function");
    expect(err).toBeDefined();
    expect(err).toBeInstanceOf(AuthError);
  });

  it("should return the AuthError type", () => {
    const err: AuthError = new AuthError(1, "msg");
    expect(err.type()).toBe(1);
  });

  it("should return the AuthError msg", () => {
    const err: AuthError = new AuthError(1, "msg");
    expect(err.msg()).toBe("msg");
  });
});
