import { Test, TestingModule } from "@nestjs/testing";
import * as request from "supertest";
import { AppModule } from "../../../src/app.module";

describe("AppController (e2e)", () => {
  let app;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();
    console.log("ip : ", app.getHttpServer());
    await app.listen(3050);
  });

  it("login (POST) - success", async () => {
    console.log("ip : " + app.getHttpServer());
    const res = await request(app.getHttpServer())
      .post("/auth/login")
      .send({ email: "prova", password: "prova" });

    expect(res.body).toEqual({
      success: true,
      data: {
        id: 1,
        email: "prova",
        name: "name_prova",
        surname: "surname_prova",
        phoneNumber: "phone_prova",
        password: "",
        createdAt: "2022-04-20T09:34:35.139Z",
        updatedAt: "2022-04-20T09:34:35.139Z",
      },
      status: 200,
    });
  });

  it("login (POST) - fail 1", async () => {
    console.log("ip : " + app.getHttpServer());
    const res = await request(app.getHttpServer())
      .post("/auth/login")
      .send({ email: "prova", password: "wrong" });

    expect(res.body).toEqual({
      success: false,
      data: "Auth failed.",
      status: 401,
    });
  });

  it("login (POST) - fail 2", async () => {
    console.log("ip : " + app.getHttpServer());
    const res = await request(app.getHttpServer())
      .post("/auth/login")
      .send({ email: "userdoesnotexist", password: "userdoesnotexist" });

    expect(res.body).toEqual({
      success: false,
      data: "Auth failed.",
      status: 401,
    });
  });

  afterEach(async () => {
    await app.close();
  });
});
