import { Test, TestingModule } from "@nestjs/testing";
import { AuthController } from "src/modules/auth/controller/auth.controller";
import { AuthService } from "src/modules/auth/service/auth.service";
type LoginInfo = {
  username: string;
  password: string;
};

describe("AuthController", () => {
  let controller: AuthController;
  let service: AuthService;
  const fakeUsers = [
    {
      id: 1,
      email: "prova",
      name: "name_prova",
      surname: "surname_prova",
      phoneNumber: "number_prova",
      password: "prova",
      createdAt: "2022-04-19T17:53:39.260Z",
      updatedAt: "2022-04-19T17:53:39.260Z",
    },
  ];

  const mockLoginPostSuccess = {
    username: "prova",
    password: "prova",
  };
  const mockLoginPostFailed1 = {
    username: "prova",
    password: "wrong",
  };
  const mockLoginPostFailed2 = {
    username: "doesntexist",
    password: "doesntexist",
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn().mockImplementation((usr: LoginInfo) => {
              const user = fakeUsers.find(
                (fakeuser) => fakeuser.email === usr.username
              );

              if (user == null) {
                return {
                  body: {
                    data: "Auth failed",
                  },
                  success: false,
                };
              } else {
                if (user.password === usr.password) {
                  return {
                    body: {
                      data: {
                        ...fakeUsers[0],
                      },
                      success: true,
                    },
                  };
                } else {
                  return {
                    body: {
                      data: "Auth failed",
                    },
                    success: false,
                  };
                }
              }
            }),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  test("should be defined", () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  it("should be instance of AuthController", () => {
    expect(controller).toBeInstanceOf(AuthController);
  });

  it("should create a login POST - login success", async () => {
    const ret = await controller.login(mockLoginPostSuccess);
    expect(service.login).toHaveBeenCalled();
    expect(ret).toEqual({
      body: {
        success: true,
        data: {
          ...fakeUsers[0],
        },
      },
      status: 200,
    });
  });

  it("should create a login POST - login failed case 1: wrong psw", async () => {
    const ret = await controller.login(mockLoginPostFailed1);
    expect(service.login).toHaveBeenCalled();
    expect(ret).toEqual({
      body: {
        success: false,
        data: "Auth failed",
      },
      status: 401,
    });
  });

  it("should create a login POST - login failed case 2: user does not exist", async () => {
    const ret = await controller.login(mockLoginPostFailed2);
    expect(service.login).toHaveBeenCalled();
    expect(ret).toEqual({
      body: {
        success: false,
        data: "Auth failed",
      },
      status: 401,
    });
  });
});
