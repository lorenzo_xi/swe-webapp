import { Column, Model, Table } from "sequelize-typescript";

@Table
export class Users extends Model {
  @Column({ primaryKey: true })
  id: number;

  @Column({})
  email: string;

  @Column({})
  name: string;

  @Column({})
  surname: string;

  @Column({})
  phoneNumber: string;

  @Column({})
  password: string;
}
