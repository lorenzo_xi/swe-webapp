import { Address } from './Address';

export interface Data {
  from: Address;
  to: Address;
  sdp: RTCSessionDescription;
  ice: RTCIceCandidate;
}
