export class User {
  private _id: string;
  private _name: string;
  private _surname: string;
  private _email: string;
  private _phoneNumber: string;
  private _isCustomer: boolean; //true iff is a customer, otherwise user is a technican
  private _isAvailable: boolean; //true iff is a technichan is available to recive a call, otherwise user is a technican

  constructor(...args: any[]) {
    if (args.length > 1) {
      this._id = args[0];
      this._name = args[1];
      this._surname = args[2];
      this._email = args[3];
      this._phoneNumber = args[4];
      if(typeof args[5] === 'string'){
        this._isCustomer = args[5]==='true';
      }else{
        this._isCustomer = args[5];
      }
      if(typeof args[6] === 'string'){
        this._isAvailable = args[6]==='true';
      }else{
        this._isAvailable = args[6];
      }
    } else{
      const usr = args[0];
      this._id = usr.id;
      this._name = usr.name;
      this._surname = usr.surname;
      this._email = usr.email;
      this._phoneNumber = usr.phoneNumber;
      if(typeof usr.isCustomer === 'string'){
        this._isCustomer = usr.isCustomer==='true';
      }else{
        this._isCustomer = usr.isCustomer;
      }
      if(typeof usr.isAvailable === 'string'){
        this._isAvailable = usr.isAvailable==='true';
      }else{
        this._isAvailable = usr.isAvailable;
      }
    }
  }

  public id(): string {
    return this._id;
  }

  public name(): string {
    return this._name;
  }

  public surname(): string {
    return this._surname;
  }

  public email(): string {
    return this._email;
  }

  public phoneNumber(): string {
    return this._phoneNumber;
  }

  public  isCustomer(): boolean {
    return this._isCustomer;
  }

  public isAvailable(): boolean {
    return this._isAvailable;
  }

  public setId(value: string) {
    this._id = value;
  }

  public setName(value: string) {
    this._name = value;
  }

  public setSurname(value: string) {
    this._surname = value;
  }

  public setEmail(value: string) {
    this._email = value;
  }

  public setPhoneNumber(value: string) {
    this._phoneNumber = value;
  }

  public setIsCustomer(value: boolean) {
    this._isCustomer = value;
  }

  public setIsAvailable(value: boolean) {
    this._isAvailable = value;
  }

  public generateRoomId(): string {
    return 'room' + (this._isCustomer? '-c-' : '-t-') + this._id;
  }
}