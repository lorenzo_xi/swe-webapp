import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WsResponse,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { User } from 'src/shared/User';
import {
  addUser,
  isEmpty,
  removeUser,
  checkIfUserExist,
} from 'src/shared/Utils';
import { Address } from 'src/shared/Address';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
  allowEIO3: true,
})
export class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server: Server;
  private customers: Array<User>;
  private technicians: Array<User>;

  private logger: Logger = new Logger('Server');

  afterInit() {
    this.customers = [];
    this.technicians = [];
    this.logger.log('Server initialized');
  }

  handleConnection(client: Socket, ...args: any[]) {
    try {
      const tmp: any = client.handshake.query;
      console.log(tmp)


      if (tmp.id.length > 0) {
        const usr: User = new User(tmp);
        const room: string = usr.generateRoomId();

        if(usr.id() === '2'){
          usr.setIsCustomer(true)
        }

        console.log(usr)


        if (usr.isCustomer()) {
          this.logger.debug(
            'connected user customer: ' +
              usr.id() +
              ', join room: ' +
              usr.generateRoomId(),
          );
          addUser(usr, this.customers);
        } else {
          addUser(usr, this.technicians);
          this.logger.debug(
            'connected user technician: ' +
              usr.id() +
              ', join room: ' +
              usr.generateRoomId(),
          );
        }
        client.join(room);
        return { event: 'connection', success: true };
      }
     
    } catch {
      this.logger.error('Error handling connection');
      return { event: 'connection', success: false };
    }
  }

  handleDisconnect(client: Socket) {
    try {
      const tmp: any = client.handshake.query;
      const usr: User = new User(tmp);
      this.logger.debug(
        'disconnected user: ' + usr.id() + ', room: ' + usr.generateRoomId(),
      );

      if (usr.isCustomer()) {
        removeUser(usr.id(), this.customers);
      } else {
        removeUser(usr.id(), this.technicians);
      }
    } catch {
      this.logger.error('Error handling disconnection');
    }
  }

  @SubscribeMessage('start-call')
  handleStartCall(client: Socket, data: any): WsResponse<any> {
    try {
      console.log('start-call!!!', data);
      const customer: Address = new Address(data.from.id, data.from.isCustomer);

      console.log("techss: " , this.technicians)
      if (this.technicians.length > 0) {

        const matchedTechnicians = this.technicians.shift();
        const technician: Address = new Address(matchedTechnicians.id(),matchedTechnicians.isCustomer());

        const i_user = checkIfUserExist(customer.id(), this.customers);
        const c_usr: User = this.customers[i_user];
        console.log("customer ", customer)

        console.log("c-usr ", c_usr)
        const obj: any = {
          id: c_usr.id(),
          name: c_usr.name(),
          surname: c_usr.surname(),
          email: c_usr.email(),
          number: c_usr.phoneNumber(),
          isCustomer: c_usr.isCustomer(),
          isAvaible: c_usr.isAvailable(),
        };
        console.log("address" , technician)
        console.log('len>0 ' + ' ' + technician.generateRoomId());

        client
          .to(technician.generateRoomId())
          .emit('start-call', { from: obj, to: technician });
      } else {
        client
          .to(customer.generateRoomId())
          .emit('no-technician', { from: data.from, to: customer });
      }
      console.log("return")
      return { event: 'start-call', data: true };
    } catch (e) {
      console.log(e)
      return { event: 'start-call', data: false };
    }
  }

  @SubscribeMessage('refuse-call')
  handleRefuseCall(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);

      client.to(user_to.generateRoomId()).emit('refuse-call', data);

      return { event: 'refuse-call', data: true };
    } catch (e) {
      return { event: 'refuse-call', data: false };
    }
  }

  @SubscribeMessage('accept-call')
  handleAcceptCall(client: Socket, data: any): WsResponse<any> {
    try {
      console.log('[accept-call]');
      console.log(data);
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);


      client.to(user_to.generateRoomId()).emit('accept-call', data);

      console.log(
        '[accept-call] from:' +
          user_from.generateRoomId() +
          ', to:' +
          user_to.generateRoomId(),
      );

      removeUser(user_from.id(), this.technicians);

      return { event: 'accept-call', data: true };
    } catch (e) {
      console.log('catch-accept-call');
      return { event: 'accept-call', data: false };
    }
  }

  @SubscribeMessage('client-offer')
  handleClientOffer(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);


      this.logger.debug(
        '[client-offer] from: ' +
          user_from.generateRoomId() +
          ', mathced tech: ' +
          user_to.generateRoomId(),
      );
      client.to(user_to.generateRoomId()).emit('client-offer', data);

      return { event: 'client-offer', data: true };
    } catch (e) {
      return { event: 'client-offer', data: false };
    }
  }

  @SubscribeMessage('technician-offer')
  handleTechnichanOffer(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);


      this.logger.debug(
        '[technician-offer] from: ' +
          user_from.generateRoomId() +
          ', mathced tech: ' +
          user_to.generateRoomId(),
      );

      client.to(user_to.generateRoomId()).emit('technician-offer', data);

      return { event: 'client-offer', data: true };
    } catch (e) {
      return { event: 'client-offer', data: false };
    }
  }

  @SubscribeMessage('client-ice-candidate')
  handleClientIceCandidate(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);


      console.log('client-ice-candidate');

      client.to(user_to.generateRoomId()).emit('client-ice-candidate', data);

      return { event: 'client-offer', data: true };
    } catch (e) {
      return { event: 'client-offer', data: false };
    }
  }

  @SubscribeMessage('technician-ice-candidate')
  handleTechnicianIceCandidate(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);

      console.log('technician-ice-candidate');

      client
        .to(user_to.generateRoomId())
        .emit('technician-ice-candidate', data);

      return { event: 'client-offer', data: true };
    } catch (e) {
      return { event: 'client-offer', data: false };
    }
  }

  @SubscribeMessage('close-call')
  closeCallTechnician(client: Socket, data: any): WsResponse<any> {
    try {
      const user_from: Address = new Address(data.from.id, data.from.isCustomer);
      const user_to: Address = new Address(data.to.id, data.to.isCustomer);

      client.to(user_to.generateRoomId()).emit('close-call', data);

      if (user_from.isCustomer) {
        removeUser(user_to.id(), this.technicians);
      } else {
        removeUser(user_from.id(), this.technicians);
      }

      return { event: 'close-call', data: true };
    } catch (e) {
      return { event: 'close-call', data: false };
    }
  }

  @SubscribeMessage('update-availability')
  async updateAvailability(
    client: Socket,
    data: any,
  ): Promise<WsResponse<any>> {
    try {
      const user_from: User = new User(data.from);

      if (data.avaiability === true) {
        addUser(user_from, this.technicians);
      } else {
        removeUser(user_from.id(), this.technicians);
      }

      return { event: 'update-availability', data: true };
    } catch (e) {
      return { event: 'update-availability', data: false };
    }
  }
}
