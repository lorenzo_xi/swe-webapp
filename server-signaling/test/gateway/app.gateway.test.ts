import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import { AppGateway } from 'src/gateway/app.gateway';
import { io, Socket } from 'socket.io-client';

describe('Test suite for AppGateway class', () => {
  let gateway: AppGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    gateway = module.get<AppGateway>(AppGateway);
  });

  it('AppGateway - should be defined', () => {
    expect(gateway).toBeDefined;
  });

  it('AppGateway - should be an intance of AppGateway', () => {
    expect(gateway).toBeInstanceOf(AppGateway);
  });

  it('handleConnection should be a function', () => {
    expect(typeof gateway.handleConnection).toBe('function');
  });

  it('handleDisconnect should be a function', () => {
    expect(typeof gateway.handleDisconnect).toBe('function');
  });

  it('handleStartCall should be a function', () => {
    expect(typeof gateway.handleStartCall).toBe('function');
  });

  it('handleRefuseCall should be a function', () => {
    expect(typeof gateway.handleRefuseCall).toBe('function');
  });

  it('handleAcceptCall should be a function', () => {
    expect(typeof gateway.handleAcceptCall).toBe('function');
  });

  it('handleClientOffer should be a function', () => {
    expect(typeof gateway.handleClientOffer).toBe('function');
  });

  it('handleTechnichanOffer should be a function', () => {
    expect(typeof gateway.handleTechnichanOffer).toBe('function');
  });

  it('handleClientIceCandidate should be a function', () => {
    expect(typeof gateway.handleClientIceCandidate).toBe('function');
  });

  it('handleTechnicianIceCandidate should be a function', () => {
    expect(typeof gateway.handleTechnicianIceCandidate).toBe('function');
  });

  it('closeCallTechnician should be a function', () => {
    expect(typeof gateway.closeCallTechnician).toBe('function');
  });

  it('updateAvailability should be a function', () => {
    expect(typeof gateway.updateAvailability).toBe('function');
  });
});


describe("my awesome project", () => {
  let app: INestApplication;
  let connectToSocketIO: () => Socket;
  let serverSocket, clientSocket;
  const mockUser = {
    id: "1",
    name: "name",
    surname: "surname",
    email: "email",
    phoneNumber: "phoneNumber",
    isCustomer: "true",
    isAvailable: "false"
  }

  beforeEach((async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    await app.listen(3000);
    const httpServer = app.getHttpServer();
    connectToSocketIO = () =>
      io(`http://127.0.0.1:${httpServer.address().port}`, {
        transports: ['websocket'],
        forceNew: true,
        query: {...mockUser},
      });

    clientSocket = connectToSocketIO();

  }),30000);

  afterEach(async () => {
    await app.close();
  });

  it("should handle succesfully the connection", ((done) => {
    console.log(clientSocket)
    expect(1).toBe(1);
  }),30000);

  it("should handle succesfully the disconnection", (done) => {
    const ret = clientSocket.emit("disconnect");
    expect(ret).toBe({ event: 'disconnect', data: true });
  });

  it("should handle succesfully start-call message", (done) => {
    const ret = clientSocket.emit("start-call");
    expect(ret).toBe({ event: 'start-call', data: true });
  });

  it("should handle succesfully refuse-call message", (done) => {
    const ret = clientSocket.emit("refuse-call");
    expect(ret).toBe({ event: 'refuse-call', data: true });
  });

  it("should handle succesfully client-offer message", (done) => {
    const ret = clientSocket.emit("client-offer");
    expect(ret).toBe({ event: 'client-offer', data: true });
  });

  it("should handle succesfully technician-offer message", (done) => {
    const ret = clientSocket.emit("technician-offer");
    expect(ret).toBe({ event: 'technician-offee', data: true });
  });

  it("should handle succesfully client-ice-candidate message", (done) => {
    const ret = clientSocket.emit("client-ice-candidate");
    expect(ret).toBe({ event: 'client-ice-candidate', data: true });
  });

  it("should handle succesfully technician-ice-candidate", (done) => {
    const ret = clientSocket.emit("technician-ice-candidate");
    expect(ret).toBe({ event: 'technician-ice-candidatee', data: true });
  });

  it("should handle succesfully client-ice-candidate message", (done) => {
    const ret = clientSocket.emit("client-ice-candidate");
    expect(ret).toBe({ event: 'technician-offer', data: true });
  });

  it("should handle succesfully close-call message", (done) => {
    const ret = clientSocket.emit("close-call");
    expect(ret).toBe({ event: 'close-call', data: true });
  });

});
