import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { io, Socket } from 'socket.io-client';

describe('Test', () => {
  let app: INestApplication;
  let connectToSocketIO: () => Socket;

  //prima di ogni test istanzia un server
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    await app.listen(0);
    const httpServer = app.getHttpServer();
    connectToSocketIO = () =>
      io(`http://127.0.0.1:${httpServer.address().port}`, {
        transports: ['websocket'],
        forceNew: true,
      });
  });

  //dopo ogni test chiude il server
  afterEach(async () => {
    await app.close();
  });

  it('should connect and disconnect', (done) => {
    const socket = connectToSocketIO();

    socket.on('connect', () => {
      socket.disconnect();
    });

    socket.on('disconnect', (reason) => {
      expect(reason).toBe('io client disconnect');
      done();
    });
    socket.on('error', done);
  });

  it('should send a support-request-start', (done) => {
    console.log('to-do');
  });

  test('should send a support-request-accept', (done) => {
    console.log('to-do');
  });

  test('should send a support-request-end', (done) => {
    console.log('to-do');
  });

  test('should send a support-request-ice-candidate', (done) => {
    console.log('to-do');
  });
});
