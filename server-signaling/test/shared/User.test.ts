import { User } from 'src/shared/User';

describe('Test suite for Address class', () => {
  const obj = {
    name: 'name',
    surname: 'surname',
    email: 'email',
    phoneNumber: 'number',
    id: '1',
    isCustomer: 'false',
    isAvailable: 'false',
  };
  const obj2 = {
    name: 'name',
    surname: 'surname',
    email: 'email',
    phoneNumber: 'number',
    id: '1',
    isCustomer: true,
    isAvailable: true,
  };
  const usr_one_par: User = new User(obj);
  const usr_more_par: User = new User(
    obj.id,
    obj.name,
    obj.surname,
    obj.email,
    obj.phoneNumber,
    obj.isCustomer,
    obj.isAvailable,
  );
  const another_user: User = new User(obj2);


  test('defines id() function', () => {
    expect(typeof usr_one_par.id).toBe('function');
  });
  test('defines name() function', () => {
    expect(typeof usr_one_par.name).toBe('function');
  });
  test('defines surname() function', () => {
    expect(typeof usr_one_par.surname).toBe('function');
  });
  test('defines email() function', () => {
    expect(typeof usr_one_par.email).toBe('function');
  });
  test('defines number() function', () => {
    expect(typeof usr_one_par.phoneNumber).toBe('function');
  });
  test('defines isCustomer() function', () => {
    expect(typeof usr_one_par.isCustomer).toBe('function');
  });
  test('defines generateRoomId() function', () => {
    expect(typeof usr_one_par.generateRoomId).toBe('function');
  });
  test('defines setId(string) function', () => {
    expect(typeof usr_one_par.setId).toBe('function');
  });
  test('defines setName() function', () => {
    expect(typeof usr_one_par.setName).toBe('function');
  });
  test('defines setSurname() function', () => {
    expect(typeof usr_one_par.setSurname).toBe('function');
  });
  test('defines setEmail() function', () => {
    expect(typeof usr_one_par.setEmail).toBe('function');
  });
  test('defines setNumber() function', () => {
    expect(typeof usr_one_par.setPhoneNumber).toBe('function');
  });
  test('defines setIsCustomer() function', () => {
    expect(typeof usr_one_par.setIsCustomer).toBe('function');
  });
  test('defines setIsAvailable() function', () => {
    expect(typeof usr_one_par.setIsAvailable).toBe('function');
  });

  it('should construct an Address object based on the passed param (only string in the passed object)', () => {
    const tmp = new User(obj);
    expect(tmp.id()).toBe(obj.id);
    expect(tmp.name()).toBe(obj.name);
    expect(tmp.surname()).toBe(obj.surname);
    expect(tmp.email()).toBe(obj.email);
    expect(tmp.phoneNumber()).toBe(obj.phoneNumber);
    expect(tmp.isCustomer()).toBe(obj.isCustomer==='true');
    expect(tmp.isAvailable()).toBe(obj.isAvailable==='true');
    expect(tmp).toBeInstanceOf(User);
  });
  it('should construct an Address object based on the passed param (not only string in the passed object, also boleans)', () => {
    const tmp = new User(obj2);
    expect(tmp.id()).toBe(obj2.id);
    expect(tmp.name()).toBe(obj2.name);
    expect(tmp.surname()).toBe(obj2.surname);
    expect(tmp.email()).toBe(obj2.email);
    expect(tmp.phoneNumber()).toBe(obj2.phoneNumber);
    expect(tmp.isCustomer()).toBe(obj2.isCustomer);
    expect(tmp.isAvailable()).toBe(obj2.isAvailable);
    expect(tmp).toBeInstanceOf(User);
  });
  it('should construct an Address object based on the two passed params (only string in the passed params)', () => {
    const tmp: User = new User(
      obj.id,
      obj.name,
      obj.surname,
      obj.email,
      obj.phoneNumber,
      obj.isCustomer,
      obj.isAvailable,
    );
    expect(tmp.id()).toBe(obj.id);
    expect(tmp.name()).toBe(obj.name);
    expect(tmp.surname()).toBe(obj.surname);
    expect(tmp.email()).toBe(obj.email);
    expect(tmp.phoneNumber()).toBe(obj.phoneNumber);
    expect(tmp.isCustomer()).toBe(obj.isCustomer==='true');
    expect(tmp.isAvailable()).toBe(obj.isAvailable==='true');
    expect(tmp).toBeInstanceOf(User);
  });
  it('should construct an Address object based on the two passed params (not only string in the passed params, also boleans)', () => {
    const tmp: User = new User(
      obj2.id,
      obj2.name,
      obj2.surname,
      obj2.email,
      obj2.phoneNumber,
      obj2.isCustomer,
      obj2.isAvailable,
    );
    expect(tmp.id()).toBe(obj2.id);
    expect(tmp.name()).toBe(obj2.name);
    expect(tmp.surname()).toBe(obj2.surname);
    expect(tmp.email()).toBe(obj2.email);
    expect(tmp.phoneNumber()).toBe(obj2.phoneNumber);
    expect(tmp.isCustomer()).toBe(obj2.isCustomer);
    expect(tmp.isAvailable()).toBe(obj2.isAvailable);
    expect(tmp).toBeInstanceOf(User);
  });

  it('should return the id attribute', () => {
    expect(usr_one_par.id()).toBe('1');
  });
  it('should return the name attribute', () => {
    expect(usr_one_par.name()).toBe('name');
  });
  it('should return the surname attribute', () => {
    expect(usr_one_par.surname()).toBe('surname');
  });
  it('should return the email attribute', () => {
    expect(usr_one_par.email()).toBe('email');
  });
  it('should return the number attribute', () => {
    expect(usr_one_par.phoneNumber()).toBe('number');
  });
  it('should return the isCustomer attribute', () => {
    expect(usr_one_par.isCustomer()).toBe(false);
  });
  it('should return the isAvailable attribute', () => {
    expect(usr_one_par.isAvailable()).toBe(false);
  });
  it('should return the generated room id (user is not a customer)', () => {
    expect(usr_one_par.generateRoomId()).toBe('room-t-1');
  });
  it('should return the generated room id (user is a customer)', () => {
    expect(another_user.generateRoomId()).toBe('room-c-1');
  });


  it('should set the id attribute', () => {
    const tmp = new User(obj);
    tmp.setId('2');
    expect(tmp.id()).toBe('2');
  });
  it('should set the name attribute', () => {
    const tmp = new User(obj);
    tmp.setName('new_name');
    expect(tmp.name()).toBe('new_name');
  });
  it('should set the surname attribute', () => {
    const tmp = new User(obj);
    tmp.setSurname('new_surname');
    expect(tmp.surname()).toBe('new_surname');
  });
  it('should set the email attribute', () => {
    const tmp = new User(obj);
    tmp.setEmail('new_email');
    expect(tmp.email()).toBe('new_email');
  });
  it('should set the number attribute', () => {
    const tmp = new User(obj);
    tmp.setPhoneNumber('new_phone');
    expect(tmp.phoneNumber()).toBe('new_phone');
  });
  it('should set the isCustomer attribute', () => {
    const tmp = new User(obj);
    tmp.setIsCustomer(true);
    expect(tmp.isCustomer()).toBeTruthy();
  });
  it('should set the isAvailable attribute', () => {
    const tmp = new User(obj);
    tmp.setIsAvailable(false);
    expect(tmp.isAvailable()).toBeFalsy();
  });
});