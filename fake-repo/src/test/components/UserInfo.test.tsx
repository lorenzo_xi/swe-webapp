import { render, screen } from "@testing-library/react";
import UserInfo, {
  UserInfoModel,
} from "src/view/components/sidebar/user-info/UserInfo";

describe("<UserInfo/>", () => {
  const renderInfoCliente = (props: Partial<UserInfoModel> = {}) => {
    const defaultProps: UserInfoModel = {
      username: "MarioRossi00",
      name: "Mario",
      surname: "Rossi",
    };
    return <UserInfo {...defaultProps} {...props} />;
  };

  test("renders username", () => {
    render(renderInfoCliente());

    const renderedUsername = screen.getByText("MarioRossi00");
    expect(renderedUsername).toBeInTheDocument();
  });

  test("renders name", () => {
    render(renderInfoCliente());

    const renderedName = screen.getByText("Mario");
    expect(renderedName).toBeInTheDocument();
  });

  test("renders surname", () => {
    render(renderInfoCliente());

    const renderedSurname = screen.getByText("Rossi");
    expect(renderedSurname).toBeInTheDocument();
  });
});
