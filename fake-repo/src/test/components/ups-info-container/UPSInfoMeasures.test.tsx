import { render, screen } from "@testing-library/react";
import UPSInfoMeasures, {
  UPSInfoMeasuresModel,
} from "src/view/components/call-in-progress/ups-info-container/UPSInfoMeasures";

describe("<UPSInfoMeasures/>", () => {
  const renderUPSInfoMeasures = (props: Partial<UPSInfoMeasuresModel> = {}) => {
    const defaultProps: UPSInfoMeasuresModel = {
      data: {
        input: [
          { number: 1, name: "InputTest1", value: "10", unitMeasure: "V" },
        ],
        output: [
          { number: 2, name: "OutputTest2", value: "20", unitMeasure: "A" },
        ],
        battery: [
          { number: 3, name: "BatteryTest3", value: "30", unitMeasure: "B" },
        ],
        inverter: [
          { number: 4, name: "InverterTest4", value: "40", unitMeasure: "I" },
        ],
        bypass: [
          { number: 5, name: "BypassTest5", value: "50", unitMeasure: "BY" },
        ],
      },
    };
    return <UPSInfoMeasures {...defaultProps} {...props} />;
  };

  test("renders input, output, battery, inverter, bypass tabs", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const inputTab = tabElementList.find(
      (element) => element.textContent === "input"
    );
    expect(inputTab).toBeInTheDocument();
    const outputTab = tabElementList.find(
      (element) => element.textContent === "output"
    );
    expect(outputTab).toBeInTheDocument();
    const batteryTab = tabElementList.find(
      (element) => element.textContent === "battery"
    );
    expect(batteryTab).toBeInTheDocument();
    const inverterTab = tabElementList.find(
      (element) => element.textContent === "inverter"
    );
    expect(inverterTab).toBeInTheDocument();
    const bypassTab = tabElementList.find(
      (element) => element.textContent === "bypass"
    );
    expect(bypassTab).toBeInTheDocument();
  });

  test("renders input measures when input tab is selected", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const inputTab = tabElementList.find(
      (element) => element.textContent === "input"
    );
    inputTab?.click();

    const numberInputElement = screen.getByText("1");
    expect(numberInputElement).toBeInTheDocument();
    const nameInputElement = screen.getByText("InputTest1");
    expect(nameInputElement).toBeInTheDocument();
    const valueInputElement = screen.getByText("10");
    expect(valueInputElement).toBeInTheDocument();
    const unitMeasureInputElement = screen.getByText("V");
    expect(unitMeasureInputElement).toBeInTheDocument();
  });

  test("renders output measures when output tab is selected", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const outputTab = tabElementList.find(
      (element) => element.textContent === "output"
    );
    outputTab?.click();

    const numberOutputElement = screen.getByText("2");
    expect(numberOutputElement).toBeInTheDocument();
    const nameOutputElement = screen.getByText("OutputTest2");
    expect(nameOutputElement).toBeInTheDocument();
    const valueOutputElement = screen.getByText("20");
    expect(valueOutputElement).toBeInTheDocument();
    const unitMeasureOutputElement = screen.getByText("A");
    expect(unitMeasureOutputElement).toBeInTheDocument();
  });

  test("renders battery measures when battery tab is selected", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const batteryTab = tabElementList.find(
      (element) => element.textContent === "battery"
    );
    batteryTab?.click();

    const numberBatteryElement = screen.getByText("3");
    expect(numberBatteryElement).toBeInTheDocument();
    const nameBatteryElement = screen.getByText("BatteryTest3");
    expect(nameBatteryElement).toBeInTheDocument();
    const valueBatteryElement = screen.getByText("30");
    expect(valueBatteryElement).toBeInTheDocument();
    const unitMeasureBatteryElement = screen.getByText("B");
    expect(unitMeasureBatteryElement).toBeInTheDocument();
  });

  test("renders inverter measures when inverter tab is selected", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const inverterTab = tabElementList.find(
      (element) => element.textContent === "inverter"
    );
    inverterTab?.click();

    const numberInverterElement = screen.getByText("4");
    expect(numberInverterElement).toBeInTheDocument();
    const nameInverterElement = screen.getByText("InverterTest4");
    expect(nameInverterElement).toBeInTheDocument();
    const valueInverterElement = screen.getByText("40");
    expect(valueInverterElement).toBeInTheDocument();
    const unitMeasureInverterElement = screen.getByText("I");
    expect(unitMeasureInverterElement).toBeInTheDocument();
  });

  test("renders bypass measures when bypass tab is selected", () => {
    render(renderUPSInfoMeasures());

    const tabElementList = screen.getAllByRole("tab");
    const bypassTab = tabElementList.find(
      (element) => element.textContent === "bypass"
    );
    bypassTab?.click();

    const numberBypassElement = screen.getByText("5");
    expect(numberBypassElement).toBeInTheDocument();
    const nameBypassElement = screen.getByText("BypassTest5");
    expect(nameBypassElement).toBeInTheDocument();
    const valueBypassElement = screen.getByText("50");
    expect(valueBypassElement).toBeInTheDocument();
    const unitMeasureBypassElement = screen.getByText("BY");
    expect(unitMeasureBypassElement).toBeInTheDocument();
  });

  test("does not render output measures when input is selected", () => {});
});
