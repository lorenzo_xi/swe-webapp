import { render, screen } from "@testing-library/react";
import UPSInfoContainer, {
  UPSInfoContainerModel,
} from "src/view/components/call-in-progress/UPSInfoContainer";

describe("<UPSInfoContainer/>", () => {
  const renderUPSInfoContainer = (
    props: Partial<UPSInfoContainerModel> = {}
  ) => {
    const defaultProps: UPSInfoContainerModel = {
      states: [{ number: 100, name: "Stato1", value: "0" }],
      alarms: [{ number: 2, name: "Allarme2", value: "5" }],
      measures: {
        input: [
          { number: 1, name: "InputTest1", value: "10", unitMeasure: "V" },
        ],
      },
    };
    return <UPSInfoContainer {...defaultProps} {...props} />;
  };

  test("render states, alarms and measure tabs", () => {
    render(renderUPSInfoContainer());

    const tabElementList = screen.getAllByRole("tab");
    const statesTab = tabElementList.find(
      (element) => element.textContent === "States"
    );
    expect(statesTab).toBeInTheDocument();
    const alarmsTab = tabElementList.find(
      (element) => element.textContent === "Alarms"
    );
    expect(alarmsTab).toBeInTheDocument();
    const measuresTab = tabElementList.find(
      (element) => element.textContent === "Measures"
    );
    expect(measuresTab).toBeInTheDocument();
  });

  test("render state values when state tab is selected", () => {
    render(renderUPSInfoContainer());

    const tabElementList = screen.getAllByRole("tab");
    const statesTab = tabElementList.find(
      (element) => element.textContent === "States"
    );
    statesTab?.click();

    const numberStateElement = screen.getByText("100");
    expect(numberStateElement).toBeInTheDocument();
    const nameStateElement = screen.getByText("Stato1");
    expect(nameStateElement).toBeInTheDocument();
    const valueStateElement = screen.getByText("0");
    expect(valueStateElement).toBeInTheDocument();
  });

  test("render alarms values when alarm tab is selected", () => {
    render(renderUPSInfoContainer());

    const tabElementList = screen.getAllByRole("tab");
    const alarmsTab = tabElementList.find(
      (element) => element.textContent === "Alarms"
    );
    alarmsTab?.click();

    const numberAlarmElement = screen.getByText("2");
    expect(numberAlarmElement).toBeInTheDocument();
    const nameAlarmElement = screen.getByText("Allarme2");
    expect(nameAlarmElement).toBeInTheDocument();
    const valueAlarmElement = screen.getByText("5");
    expect(valueAlarmElement).toBeInTheDocument();
  });

  test("render measures values when measure tab is selected", () => {
    render(renderUPSInfoContainer());

    const tabElementList = screen.getAllByRole("tab");
    const measuresTab = tabElementList.find(
      (element) => element.textContent === "Measures"
    );
    measuresTab?.click();

    const inputTab = tabElementList.find(
      (element) => element.textContent === "input"
    );
    expect(inputTab).toBeInTheDocument();
  });
});
