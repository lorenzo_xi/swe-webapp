import { render, screen } from "@testing-library/react";
import UPSInfoStatesOrAlarms, {
  UPSInfoStatesOrAlarmsModel,
} from "src/view/components/call-in-progress/ups-info-container/UPSInfoStatesOrAlarms";

describe("<UPSInfoStatesOrAllarms/>", () => {
  const renderUPSInfoStatesOrAllarms = (
    props: Partial<UPSInfoStatesOrAlarmsModel> = {}
  ) => {
    const defaultProps: UPSInfoStatesOrAlarmsModel = {
      data: [{ number: 1, name: "Stato1", value: "0" }],
    };
    return <UPSInfoStatesOrAlarms {...defaultProps} {...props} />;
  };

  test("renders states or allarms values", () => {
    render(renderUPSInfoStatesOrAllarms());

    const numberElement = screen.getByText("1");
    expect(numberElement).toBeInTheDocument();
    const nameElement = screen.getByText("Stato1");
    expect(nameElement).toBeInTheDocument();
    const valueElement = screen.getByText("0");
    expect(valueElement).toBeInTheDocument();
  });
});
