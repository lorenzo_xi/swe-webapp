import { Row, Col, Form, FormCheck, Container } from 'react-bootstrap';
import IconMenu from '../components/sidebar/IconMenu';
import { ArrowLeft, Wifi } from 'react-feather';
import TechnicianDetails from '../components/main-container/TechnicianDetails';
import UPSUserConnectionDetails from '../components/call-in-progress/UPSUserConnectionDetails';

function TechnicianDetailsPage() {
   return (
      <Container className='m-0 p-0 d-flex'>
         <Col md={0.5} className='p-0 m-0 d-flex min-vh-100'>
            <IconMenu />
         </Col>
         <Col md={11}>
            <Row>
               <UPSUserConnectionDetails
                  icon={<Wifi color='green' />}
                  stateConnection='good'
                  colorConnection='green'
               />
            </Row>
            <Row>
               <Form className='ms-5 my-3 ps-4'>
                  <FormCheck
                     id='disponibility-switch'
                     type='switch'
                     label='Disponibilità'
                  />
               </Form>
            </Row>
            <Row className='mt-5 pt-5'>
               <TechnicianDetails
                  CI='0001'
                  Name='Gino'
                  Surname='Pino'
                  email='ginoPino@gmail.com'
                  number='3420833740'
               />
            </Row>
         </Col>
      </Container>
   );
}


export default TechnicianDetailsPage;

{/* <Row md={11} className='m-0 p-0'>
            <Col md={1} className='min-vh-100 m-0 p-0 d-flex flex-column align-items-center justify-content-evenly bg-dark'>
               <IconMenu />
            </Col>
            <Col md={10} className='d-flex flex-column'>
               <UPSUserConnectionDetails
                  icon={<Wifi color='green' />}
                  stateConnection='good'
                  colorConnection='green'
               />
                  <Form className='ms-5 my-3 ps-4'>
                     <FormCheck
                        id='disponibility-switch'
                        type='switch'
                        label='Disponibilità'
                        />
                  </Form>
               <Row className=' h-50 m-0 p-0 flex-column justify-content-end'>
                  <TechnicianDetails
                     CI='0001'
                     Name='Gino'
                     Surname='Pino'
                     email='ginoPino@gmail.com'
                     number='3420833740'
                     />
               </Row>
            </Col>
         </Row> */}