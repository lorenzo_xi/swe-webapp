import { Row, Col, Container } from "react-bootstrap";
import UPSInfoContainer from "../components/call-in-progress/UPSInfoContainer";
import UPSUserConnectionDetails from "../components/call-in-progress/UPSUserConnectionDetails";
import VideoSection from "../components/call-in-progress/VideoSection";
import { Wifi } from "react-feather";
import IconMenu from "../components/sidebar/IconMenu";
import SideBar from "../components/sidebar/SideBar";
const mockStati = [
   { number: 1, name: "Stato1", value: "0" },
   { number: 2, name: "Stato2", value: "1" },
   { number: 3, name: "Stato3", value: "0" },
];

const mockAllarmi = [
   { number: 4, name: "Allarme1", value: "0" },
   { number: 5, name: "Allarme2", value: "1" },
   { number: 6, name: "Allarme3", value: "0" },
];

const mockMisure = {
   input: [
      { number: 4, name: "Input1", value: "30", unitMeasure: "V" },
      { number: 5, name: "Input2", value: "10", unitMeasure: "A" },
      { number: 6, name: "Input3", value: "20", unitMeasure: "W" },
   ],
   output: [
      { number: 7, name: "Output1", value: "30", unitMeasure: "V" },
      { number: 8, name: "Output2", value: "10", unitMeasure: "A" },
      { number: 9, name: "Output3", value: "20", unitMeasure: "W" },
   ],
};


const CallInProgressPage = () => {
   return (
      <Container className='m-0 p-0 d-flex'>
         <Col md={0.5} className='p-0 m-0 d-flex min-vh-100'>
            <IconMenu />
         </Col>
         <Col md={3} className='p-2 d-flex flex-column justify-content-evenly align-items-center bg-secondary'>
            <SideBar />
         </Col>
         <Col md={9} className='p-3'>
            <Row>
               <UPSUserConnectionDetails
                  icon={<Wifi strokeWidth={2} color="green" />}
                  colorConnection="green"
                  stateConnection="good"
               />
            </Row>
            <Row className='justify-content-evenly my-4'>
               <VideoSection />
            </Row>
            <Row className='m-0'>
               <UPSInfoContainer
                  states={mockStati}
                  alarms={mockAllarmi}
                  measures={mockMisure}
               />
            </Row>
         </Col>
      </Container>
   );
}

export default CallInProgressPage;

{/* <>
<Row>
   <Col>
      <UPSUserConnectionDetails
         icon={<Wifi strokeWidth={2} color="green" />}
         colorConnection="green"
         stateConnection="good"
      />
   </Col>
</Row>

<Row className="flex-column">
   <Col className='mb-5'>
      <VideoSection />
   </Col>
   <Col>
      <UPSInfoContainer
         states={mockStati}
         alarms={mockAllarmi}
         measures={mockMisure}
      />
   </Col>
</Row>
</> */}