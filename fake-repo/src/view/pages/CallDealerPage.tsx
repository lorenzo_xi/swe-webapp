import SideBar from '../components/sidebar/SideBar'
import RejectButton from "../components/incoming-call/RejectButton";
import AcceptButton from "../components/incoming-call/AcceptButton";
import UPSUserConnectionDetails from "../components/call-in-progress/UPSUserConnectionDetails";
import VideoSection from "../components/call-in-progress/VideoSection";
import { Container, Row, Col } from "react-bootstrap";
import { Wifi } from 'react-feather';
import IconMenu from '../components/sidebar/IconMenu';
function CallDealerPage() {
   return (
      <Container className='m-0 p-0 d-flex flex-row justify-content-start'>
         <Col md={0.5} className=' p-0 m-0 d-flex min-vh-100'>
            <IconMenu/>
         </Col>
         <Col md={3} className='p-2 d-flex flex-column justify-content-evenly align-items-center bg-secondary'>
            <SideBar/>
         </Col>
         <Col md={9}>
            <Row>
               <UPSUserConnectionDetails
                  icon={<Wifi strokeWidth={2} color="green" />}
                     colorConnection="green"
                     stateConnection="good"
               />
            </Row>
            <Row className="flex-column">
            <Col className='mb-5'>
               <VideoSection />
            </Col>
            <Col>
               <Container className=" mt-2 p-2">
                  <Row>
                     <Col className="d-flex justify-content-evenly">
                        <AcceptButton name="Accept" />
                        <RejectButton name="Reject" />
                     </Col>
                  </Row>
               </Container>
            </Col>
         </Row>
         </Col>
      </Container>
   );
}

export default CallDealerPage;

// <Col md={3} className='p-0 m-0 d-flex p-0 min-vh-100 align-content-center bg-secondary'>
//             <SideBar/>
//          </Col>
//          <Col md={8}>
//          <Row>
//             <Col className='d-flex'>
//                <UPSUserConnectionDetails
//                   icon={<Wifi strokeWidth={2} color="green" />}
//                   colorConnection="green"
//                   stateConnection="good"
//                />
//             </Col>
//          </Row>

         
//          </Col>
