import { Row, Col, Container } from "react-bootstrap";
import VideoSection from "../components/call-in-progress/VideoSection";
import UPSUserConnectionDetails from "../components/call-in-progress/UPSUserConnectionDetails";
import { Wifi } from 'react-feather'
import SpinnerUI from "../components/loader/SpinnerUI";
import IconMenu from "../components/sidebar/IconMenu";
import SideBar from "../components/sidebar/SideBar";


function WaitingCallPage() {
   return (
      <Container className='m-0 p-0 d-flex'>
         <Col md={0.5} className='p-0 m-0 d-flex min-vh-100'>
            <IconMenu/>
         </Col>
         <Col md={3} className='p-2 d-flex flex-column justify-content-evenly align-items-center bg-secondary'>
            <SideBar/>
         </Col>
         <Col md={9} className='p-3'>
            <Row>
               <UPSUserConnectionDetails
                  icon={<Wifi strokeWidth={2} color="green" />}
                  colorConnection="green"
                  stateConnection="good"
               />
            </Row>
            <Row className='justify-content-evenly my-4'>
               <VideoSection/>
            </Row>
            <Row className='flex-column justify-content-center align-content-center'>
               <SpinnerUI/>
            </Row>
         </Col>
      </Container>
   );
}


export default WaitingCallPage;

{/* <>
<Row>
   <Col>
      <UPSUserConnectionDetails
         icon={<Wifi strokeWidth={2} color="green" />}
         colorConnection="green"
         stateConnection="good"
      />
   </Col>
</Row>

<Row className="flex-column">
   <Col className=' mb-5'>
      <VideoSection />
   </Col>
   <Col>
      <SpinnerUI/>
   </Col>
</Row>
</> */}


