import { render } from "@testing-library/react";
import React, { Component, FC } from "react";
import { Card } from "react-bootstrap";
import { TecnicianModel } from "./TecnicianModel";
import classes from "./Tecnico.module.css";

const Tecnician = (props: TecnicianModel) => {
  return (
    <Card className=' d-flex'>
      <Card.Header>Tecnico:</Card.Header>
      <Card.Body className='d-flex align-content-center justify-content-evenly'>
        <p>{props.id} {props.name} {props.surname}</p>
      </Card.Body>
    </Card>
  );
};

export default Tecnician;
