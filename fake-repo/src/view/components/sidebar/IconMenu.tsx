import SingleIconMenu from "./SingleIconMenu";
import { Home, User } from "react-feather";
import { Row, Col, Nav} from "react-bootstrap";
import { NavLink } from "react-router-dom";
function IconMenu() {
   return (
      <Nav className=" m-0 flex-column bg-dark align-content-center justify-content-evenly">
         <NavLink to='/Home' className='py-5 px-2'>
            <Home color='white'/>
         </NavLink>
         <NavLink to='/User' className='py-5 px-2'>
            <User color="white"/>
         </NavLink>
      </Nav>
   );

} 

export default IconMenu;
{/* <SingleIconMenu
icon={<Home color='white'/>}
link='#'

/>
<SingleIconMenu
icon={<User color='white'/>}
link='#'
/> */}