import { Col, Row } from 'react-bootstrap';

type SingleIconMenuModel = {
   icon: React.ReactNode,
   link: string,
};

function SingleIconMenu(props: SingleIconMenuModel) {
   return (
      
         <a className='d-flex align-content-center' href={props.link}>{props.icon}</a>
   );
}

export default SingleIconMenu;