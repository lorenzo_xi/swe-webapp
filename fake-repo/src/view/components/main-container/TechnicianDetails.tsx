import { Row, Col, Form } from 'react-bootstrap'

type TechinicianDetaiulsModel = {
   CI: string,
   Name: string,
   Surname: string,
   email: string,
   number: string,
};

function TechnicianDetails(props: TechinicianDetaiulsModel) {
   return (
      <>
         <p className='ps-5 my-3 ms-5 pe-0'>
            <strong>
               CI: 
            </strong>
            {props.CI}
         </p>
         <p className=' ps-5 my-3 ms-5 pe-0'>
            <strong>
               Name: 
            </strong>
            {props.Name}
         </p>
         <p className=' ps-5 my-3 ms-5 pe-0'>
            <strong>
               Surname: 
            </strong>
            {props.Surname}
         </p>
         <p className=' ps-5 my-3 ms-5 pe-0'>
            <strong>
               Email: 
            </strong>
            {props.email}
         </p>
         <p className=' ps-5 my-3 ms-5 pe-0'>
            <strong>
               Telephone: 
            </strong>
            {props.number}
         </p>
      </>
   );
}

export default TechnicianDetails;