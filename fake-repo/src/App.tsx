import UPSInfoContainer from "./view/components/call-in-progress/UPSInfoContainer";
// import UserInfo from "./view/components/sidebar/user-info/UserInfo";
import SideBar from "./view/components/sidebar/SideBar";
import { Row, Col } from 'react-bootstrap'
import UPSUserConnectionDetails from "./view/components/call-in-progress/UPSUserConnectionDetails";
import { Layout, Wifi } from "react-feather";
import VideoSection from "./view/components/call-in-progress/VideoSection";
import CallInProgressPage from "./view/pages/CallInProgressPage";
import WaitingCallPage from "./view/pages/WaitingCallPage";
import CallDealerPage from "./view/pages/CallDealerPage";
import TechnicianDetailsPage from "./view/pages/TechnicianDetailsPage";

import {Route, Switch, Redirect} from 'react-router-dom';

function App() {
  return (
    <Switch>
      <Route path='/Home'>
        <WaitingCallPage/>
      </Route>
      <Route path='/User'>
        <TechnicianDetailsPage/>
      </Route>
    </Switch>
  );
}

export default App;

{/* <Row md={12} className='m-0'>
  <Col md={3} className='p-0'>
    <SideBar />
  </Col>
  <Col md={9}>
    <WaitingCallPage />
  </Col>
</Row> */}