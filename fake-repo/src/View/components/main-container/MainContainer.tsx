import React, { Children } from "react";

type MainContainerModel = {
   className: string;
   children?: Array<JSX.Element>;
};


function MainContainer({className, children}: MainContainerModel) {
      className=`${className} w-100 d-flex flex-column align-item-center`;
   return(
      
      <div className={className}>
         {children}
      </div>
   );
}

// const MainContainer = ({}) => {
//    return(
//       <div className="w-75 d-flex flex-column">
//          {Children}
//       </div>
//    );
// }



export default MainContainer;