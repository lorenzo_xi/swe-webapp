import { MeasuresModel } from "./MeasuresModel";
import { Row, Col, ListGroup, ListGroupItem, Tabs, Tab } from "react-bootstrap";
import { useState } from "react";

// const search = ({type}:MeasuresModel) => {

// }


function Measures(props: MeasuresModel[]) {
   const [key, setKey] = useState('prova1');
   function search(props: MeasuresModel[], title: string) {
      let arrrayMisure: MeasuresModel[] = [];
      for (var index in props) {
         if (props[index].type === title) {
            arrrayMisure.push(props[index]);
         }
      }
      return arrrayMisure.map((item)=>{
         return(
            <ListGroupItem>
               <Row>
                  <Col sm={3}>{item.code}</Col>
                  <Col sm={6}>{item.title}</Col>
                  <Col sm={3}>{item.isActive}</Col>
               </Row>
            </ListGroupItem>
         );
      });
   }
   return (
      <Tabs>
         <Tab title='prova 1'>
            <ListGroup>
               {search(props, 'prova1')}
            </ListGroup>
         </Tab>
         <Tab title="prova 2">
            <ListGroup>
               {search(props,'prova2')}
            </ListGroup>
         </Tab>
         <Tab title="prova 3">
            <ListGroup>
               {search(props,'prova3')}
            </ListGroup>
         </Tab>
      </Tabs>
   );
}

export default Measures;