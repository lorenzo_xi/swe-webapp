export type AllarmModel ={
   code: string;
   title: string;
   isActive: boolean;
};