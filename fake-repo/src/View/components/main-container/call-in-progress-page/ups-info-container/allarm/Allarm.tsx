import { AllarmModel } from "./AllarmModel";

import { Row, Col, ListGroupItem } from "react-bootstrap";

function Allarm({code, title,isActive}: AllarmModel){
   return(
      <ListGroupItem>
         <Row>
            <Col sm={3}>{code}</Col>
            <Col sm={6}>{title}</Col>
            <Col sm={3}>{isActive}</Col>
         </Row>
      </ListGroupItem>
   );
}

export default Allarm;