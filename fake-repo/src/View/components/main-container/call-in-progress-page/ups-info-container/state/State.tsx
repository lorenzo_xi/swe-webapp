import { StateModel } from "./StateModel";

import { Row, Col, ListGroupItem } from "react-bootstrap";

function State({ code, title, isActive }: StateModel) {
   return (
      // <ListGroup>
      //    <ListGroupItem>{code} {title}  </ListGroupItem>


      // </ListGroup>
      <ListGroupItem>
         <Row>
            <Col sm={3}>{code}</Col>
            <Col sm={6}>{title}</Col>
            <Col sm={3}>{isActive}</Col>
         </Row>
      </ListGroupItem>
   );
}

export default State;
