export type StateModel={
   code: string;
   title: string;
   isActive: boolean;
};