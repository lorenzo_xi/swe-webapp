import React from "react";
import classes from "./SpinnerUI.module.css";
import { Spinner } from "react-bootstrap";
import {Row,Col } from 'react-bootstrap';
const SpinnerUI = () => {
   const divClassSpinner = `${classes.spinner} d-flex`;
   const divClassWait = `${classes.wait} d-flex justify-content-center`;

   return (
      <>
         <Col md={10} className={divClassWait}>
            <h1 className='font-weight-light'> In attesa di una chiamata...</h1>
         </Col>
         <Col md={10} className='d-flex justify-content-center'>
            <Spinner
               animation='border'
               variant='info'
            />
         </Col>
      </>
   );
};

export default SpinnerUI;
