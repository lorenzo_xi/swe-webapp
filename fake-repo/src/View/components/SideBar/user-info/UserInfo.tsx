import { Card } from "react-bootstrap";

export type UserInfoModel = {
  username: string;
  name: string;
  surname: string;
};

const UserInfo: React.FC<UserInfoModel> = ({ username, name, surname }) => {
  return (
    <Card className=" d-flex">
      <Card.Header>Client</Card.Header>
      <Card.Body>
        <div>
          Username: <strong>{username}</strong>
        </div>
        <div>
          Name: <strong>{name}</strong>
        </div>
        <div>
          Surname: <strong>{surname}</strong>
        </div>
      </Card.Body>
    </Card>
  );

  /* return (
    <Card className={classes.card}>
      <Card.Header>Client: </Card.Header>
      <Card.Body className={classes.cardBody}>
        <ul>
          <li>
            Username: <strong> {username} </strong>
          </li>
          <li>
            Surname: <strong> {surname} </strong>
          </li>
          <li>
            Name: <strong> {name} </strong>
          </li>
        </ul>
      </Card.Body>
    </Card>
  ); */
};

export default UserInfo;
