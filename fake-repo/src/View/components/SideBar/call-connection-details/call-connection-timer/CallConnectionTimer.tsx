import { CallConnectionTimerModel } from "./CallConnectionTimerModel";
import { useStopwatch } from "react-timer-hook";
import {Container, Row, Col} from 'react-bootstrap'
import { useState } from "react";
function CallConnectionTImer () {
   const {
      seconds,
      minutes,
      hours,
      start,
      reset,
   } = useStopwatch({autoStart:true});
   // da capire se metterlo qui o nel component container
   // const [timer, setTimer] = useState(0);
   return(
      <Container>
         <Row>
            <Col>
               <span>{hours}</span>:<span>{minutes}</span>:<span>{seconds}</span>
            </Col>
         </Row>
      </Container>
   );
}

export default CallConnectionTImer;