import React, { Children, Component } from "react";
import { Card, Row, Col, Container } from "react-bootstrap";
import ButtonUI from "../UI/buttonUI/ButtonUI";
import UserInfo from "./user-info/UserInfo";
import classes from './SideBar.module.css';
import Button from "../Button/Button";
import Tecnician from "./tecnician-info/TecnicianInfo";
import { LogOut } from "react-feather";
import IconMenu from "./IconMenu";


const SideBar = () => {
   return (
      <>
         <Row>
            <Tecnician
               id={101}
               name="Gino"
               surname="Pino"
            />
         </Row>
         <Row>
            <UserInfo
               username="ciccio"
               name="gino"
               surname="Pasticcio"
            />
         </Row>
         <Row className='w-50'>
            <ButtonUI
               
               icon={<LogOut />}
               
               type="button"
               variant="danger"
               onClick={() => { alert('logout') }}
               >Logout</ButtonUI>
               </Row>

      </>

   );
};
export default SideBar;

{/* <Col md={3} className='p-0 bg-dark d-flex flex-column justify-content-evenly align-content-center'>
<IconMenu />
</Col>
<Col md={7} className='min-vh-100 d-flex align-items-center flex-column justify-content-around align-content-center'>
<Row md={11} className='align-items-center'>

   <Tecnician
      id={101}
      name="Gino"
      surname="Pino"
   />
</Row>
<Row>

   <UserInfo
      username="ciccio"
      name="gino"
      surname="Pasticcio"
   />
</Row>
<Row>
   <ButtonUI
      icon={<LogOut />}
      size='lg'
      type="button"
      variant="danger"
      onClick={() => { alert('logout') }}
   >Logout</ButtonUI>
</Row>
</Col> */}