import ButtonUI from '../UI/buttonUI/ButtonUI';
import {Phone} from 'react-feather'

export type AcceptButtonModel = {
   name: string;
};

function AcceptButton ({name}: AcceptButtonModel){
   return(
      <ButtonUI
         onClick={()=>{alert("accetta")}}
         icon={<Phone/>}
         size="lg"
         type="button"
         variant="success"
      >{name}</ButtonUI>
   );
}

export default AcceptButton;