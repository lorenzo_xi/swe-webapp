import ButtonUI from '../UI/buttonUI/ButtonUI';
import {PhoneOff} from 'react-feather'

export type RejectButtonModel = {
   name: string;
};

function RejectButton ({name}:RejectButtonModel){
   return(
      <ButtonUI
         onClick={()=>{alert("rifiuta")}}
         icon={<PhoneOff />}
         size="lg"
         type="button"
         variant="danger"
      >{name}</ButtonUI>
   );
}

export default RejectButton;