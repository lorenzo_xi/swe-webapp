import { MouseEventHandler } from "react";

export type ButtonUIModel = {
    onClick: MouseEventHandler<HTMLButtonElement>;
    icon?: React.ReactNode;
    outline?: boolean;
    size?: "lg" | "sm";
    type?: "button" | "submit" | "reset";
    variant?:
      | "primary"
      | "secondary"
      | "success"
      | "warning"
      | "danger"
      | "info"
      | "light"
      | "dark"
      | "link";
  };