import { ButtonUIModel } from "./ButtonUIModel";
import { Button } from "react-bootstrap";

const ButtonUI: React.FC<ButtonUIModel> = ({
  children,
  icon = undefined,
  onClick = () => {},
  outline = false,
  size = undefined,
  type = "button",
  variant = "primary",
  ...rest
}) => {
  const buttonVariant = outline ? `outline-${variant}` : `${variant}`;

  return (
    <Button size={size} type={type} variant={buttonVariant} onClick={onClick} {...rest}>
      <span className="d-flex align-items-center">
        <span className="mr-2"> 
        {icon}
        </span>
        {children}
      </span>
    </Button>
  );
};

export default ButtonUI;
