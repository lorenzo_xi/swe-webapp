import { Card as BSCard } from "react-bootstrap";

type CardModel = {
    className?: string;
    shadowSize?: "sm" | "md" | "lg";
}

const Card : React.FC<CardModel> = ({children, className="", shadowSize="md"}) => {
    let classname = `${className} bg-white rounded `
    switch(shadowSize){
        case "sm":
            classname += "shadow-sm"
            break
        case "lg":
            classname += "shadow-lg"
            break
        default:
            classname += "shadow"
    }

    return <BSCard className={classname}>{children}</BSCard>
}

export default Card