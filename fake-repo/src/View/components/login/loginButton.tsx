import ButtonUI from '../UI/buttonUI/ButtonUI'

interface LoginButtonModel extends React.HTMLAttributes<HTMLButtonElement>  {
    loginInfo?: Object
}

const LoginButton : React.FC<LoginButtonModel> = ({children, loginInfo, ...rest}) => {
  const onClick = (data: any) => {console.log(data)};
  return <ButtonUI type='submit' onClick={onClick} {...rest}>{children}</ButtonUI>;
};

export default LoginButton;
