import { Form } from "react-bootstrap";
import Card from "../UI/card/Card";
import LoginButton from "./loginButton";

const LoginBox = () => {
  const onSubmit = (data: any) => {data.preventDefault(); console.log(data)};

  return (
    <Card className="p-2">
      <Form onSubmit={onSubmit}>

        <Form.Group>
          <Form.Label htmlFor="technician-email">Email</Form.Label>
          <Form.Control type="email" id="technician-email" />
        </Form.Group>

        <Form.Group className="my-1">
          <Form.Label htmlFor="technician-password">Password</Form.Label>
          <Form.Control type="password" id="technician-password" />
        </Form.Group>

        <LoginButton>Login</LoginButton>

      </Form>
    </Card>
  );
};

export default LoginBox;
