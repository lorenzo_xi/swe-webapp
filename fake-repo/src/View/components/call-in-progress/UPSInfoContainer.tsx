import { Row, Col, Tabs, Tab, ListGroup } from "react-bootstrap";
import { useState } from "react";
// import State from "../main-container/call-in-progress-page/ups-info-container/state/State";
// import Allarm from "../main-container/call-in-progress-page/ups-info-container/allarm/Allarm";
import UPSInfoMeasures from "./ups-info-container/UPSInfoMeasures";
import UPSInfoStatesOrAlarms from "./ups-info-container/UPSInfoStatesOrAlarms";

import { measureModel } from "./ups-info-container/UPSInfoMeasures";
import { singleStateOrAlarmModel } from "./ups-info-container/UPSInfoStatesOrAlarms";

export type UPSInfoContainerModel = {
  states: Array<singleStateOrAlarmModel>;
  alarms: Array<singleStateOrAlarmModel>;
  measures: measureModel;
}

function UPSInfoContainer({ states, alarms, measures }: UPSInfoContainerModel) {
  /* const mockStati = [
    { number: 1, name: "Stato1", value: "0" },
    { number: 2, name: "Stato2", value: "1" },
    { number: 3, name: "Stato3", value: "0" },
  ];

  const mockAllarmi = [
    { number: 4, name: "Allarme1", value: "0" },
    { number: 5, name: "Allarme2", value: "1" },
    { number: 6, name: "Allarme3", value: "0" },
  ];

  const mockMisure = {
    input: [
      { number: 4, name: "Input1", value: "30", unitMeasure: "V" },
      { number: 5, name: "Input2", value: "10", unitMeasure: "A" },
      { number: 6, name: "Input3", value: "20", unitMeasure: "W" },
    ],
    output: [
      { number: 7, name: "Output1", value: "30", unitMeasure: "V" },
      { number: 8, name: "Output2", value: "10", unitMeasure: "A" },
      { number: 9, name: "Output3", value: "20", unitMeasure: "W" },
    ],
  }; */

  return (
    <Tabs className="">
      <Tab eventKey="stati" title="Stati" className="w-75">
        <UPSInfoStatesOrAlarms data={states} />
      </Tab>
      <Tab eventKey="allarmi" title="Allarmi" className="w-75">
        <UPSInfoStatesOrAlarms data={alarms} />
      </Tab>
      <Tab eventKey="misure" title="Misure" className="w-75">
        <UPSInfoMeasures data={measures} />
      </Tab>
    </Tabs>
  );
}

export default UPSInfoContainer;
