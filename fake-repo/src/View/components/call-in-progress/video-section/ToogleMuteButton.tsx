import ButtonUI from "../../UI/buttonUI/ButtonUI";
import { Mic, MicOff } from 'react-feather'
import { useState } from "react";

export type ToogleMuteButtonModel = {
   state: boolean;
};

function ToogleMuteButton({ state }: ToogleMuteButtonModel) {
   
   const [button, setButton] =useState(false);
   return (
      <>
         {!button ? (
            <>
            <ButtonUI
               onClick={() => { setButton(true); }}
               icon={<Mic />}
               size="sm"
               type="button"
               > Attiva</ButtonUI>
            </>
         ) : (
            <>
            <ButtonUI
            onClick={() => { setButton(false) }}
            icon={<MicOff />}
               size="sm"
               type="button"
               > Disattiva</ButtonUI>
            </> 
            )
      }
      </>
   );
}


export default ToogleMuteButton;