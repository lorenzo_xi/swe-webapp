import ToogleVideoButton from "./ToogleVideoButton";
import ToogleMuteButton from "./ToogleMuteButton";
import { Row, Col, Card, Container } from 'react-bootstrap';
import React, { ElementRef } from "react";

type TecnicianVideoModel = {
   VideoState: boolean;
   MicState: boolean;
   video?: React.LegacyRef<HTMLVideoElement>;
};

// DA RIVEDERE VIDEO COME varibile di modello

function TecnicianVideo({ VideoState, MicState, video }: TecnicianVideoModel) {
   return (
         <Col md={5} className='d-flex flex-column'>
            <Card className="">Tecnico</Card>

            <Col className="d-flex justify-content-evenly">
               <ToogleMuteButton
                  state={MicState}
               />
               <ToogleVideoButton
                  state={VideoState}
               />
            </Col>
         </Col>
   );
}

export default TecnicianVideo;