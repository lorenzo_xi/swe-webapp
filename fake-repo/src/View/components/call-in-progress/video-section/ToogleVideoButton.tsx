import ButtonUI from "../../UI/buttonUI/ButtonUI";
import { Video, VideoOff } from 'react-feather'
import { useState } from "react";

export type ToogleVideoButtonModel = {
   state: boolean;
};

function ToogleVideoButton({ state }: ToogleVideoButtonModel) {
   const [button, setButton] =useState(false);

   return (
      <>
         <>
         {!button ? (
            <>
            <ButtonUI
               onClick={() => { setButton(true); }}
               icon={<Video />}
               size="sm"
               type="button"
               > Attiva</ButtonUI>
            </>
         ) : (
            <>
            <ButtonUI
            onClick={() => { setButton(false) }}
            icon={<VideoOff />}
               size="sm"
               type="button"
               > Disattiva</ButtonUI>
            </> 
            )
      }
      </>

      </>
   );
}


export default ToogleVideoButton;