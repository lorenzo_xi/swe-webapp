import {Row, Col} from "react-bootstrap"

export type UPSUserConnectionModel = {
   stateConnection:
      | "good"
      | "warning"
      | "danger";
   colorConnection:
      | "green"
      | "yellow"
      | "red"
   icon?: React.ReactNode;
};

function UPSUserConnectionDetails({stateConnection,colorConnection,icon}:UPSUserConnectionModel){
   return(
      <Row  className="m-5 mb-3 mt-3 align-content-start justify-content-end align-items-start">
         <Col md={12} className="d-flex p-1 justify-content-end">
            {icon}
            {stateConnection}
         </Col>
      </Row>
   );
}

export default UPSUserConnectionDetails;