import { Col, Row } from "react-bootstrap";

export type singleStateOrAlarmModel = {
  number: Number;
  name: String;
  value: String;
};

export type UPSInfoStatesOrAlarmsModel = {
  data: Array<singleStateOrAlarmModel>;
};

const UPSInfoStatesOrAlarms: React.FC<UPSInfoStatesOrAlarmsModel> = ({ data }) => {
  return (
    <>
      {data.map((row, index) => (
        <Row md={10} key={index}>
          <Col md="2">{row.number}</Col>
          <Col md="5">{row.name}</Col>
          <Col md="3">{row.value}</Col>
        </Row>
      ))}
    </>
  );
};

export default UPSInfoStatesOrAlarms;
