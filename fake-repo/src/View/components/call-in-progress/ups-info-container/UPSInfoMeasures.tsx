import { Tabs, Tab, Row, Col } from "react-bootstrap";

/*  STRUCTURE:   
    data : {
        Input: [
            { number, name, value },
            { number, name, value },
            { number, name, value },
        ],
        Output: [
            { number, name, value },
            { number, name, value },
            { number, name, value },    
        ]
    }
*/

type singleMeasureModel = {
  number: Number;
  name: String;
  value: String;
  unitMeasure: String;
};

export type measureModel = {
  input?: Array<singleMeasureModel>;
  output?: Array<singleMeasureModel>;
  battery?: Array<singleMeasureModel>;
  inverter?: Array<singleMeasureModel>;
  bypass?: Array<singleMeasureModel>;
};

export type UPSInfoMeasuresModel = {
  data: measureModel;
};

const UPSInfoMeasures: React.FC<UPSInfoMeasuresModel> = ({ data }) => {
  const renderMeasureTabs = (data: measureModel) => {
    const measureTabs: Array<JSX.Element> = [];
    for (let [key, value] of Object.entries(data)) {
      measureTabs.push(
        <Tab eventKey={key} title={key} key={key}>
          {value.map((row: singleMeasureModel, index) => (
            <Row key={index}>
              <Col>{row.number}</Col>
              <Col>{row.name}</Col>
              <Col>{row.value}</Col>
              <Col>{row.unitMeasure}</Col>
            </Row>
          ))}
        </Tab>
      );
    }
    return measureTabs
  };

  return <Tabs className="w-75">{renderMeasureTabs(data)}</Tabs>;
};

export default UPSInfoMeasures;
