import React from "react";
import classes from './Button.module.css'

type ButtonModel={
   id: string;
   icon: string;
   type: 
      | 'button'
      | 'submit';
   label: string;
   background: 
      | string
      | number
      | undefined;
   onclick: () => void;
}

function Button({id,icon,type,label,background,onclick}:ButtonModel){
   return(
      <button id={id} type={type} onClick={onclick} className={classes.button} style={{background}}>
         <img src={icon} alt="icon"/>
         <span className="d-flex align-items-center justify-content-center">{label}</span>
      </button>
   );
}
// style={{'background': {background}}}

export default Button;

