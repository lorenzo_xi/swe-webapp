import LoginBox from "../components/login/loginBox";

const Login = () => {
  return (
    <div className="d-flex justify-content-center mt-2 w-50">
      <LoginBox />
    </div>
  );
};

export default Login;
