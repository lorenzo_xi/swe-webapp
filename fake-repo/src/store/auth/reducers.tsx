import { AnyAction } from "@reduxjs/toolkit"
import { ActionTypes } from "./actions"

interface authState {
    name: String,
    surname: String,
    code: String,
}

const authInitialState:  authState = {
    name: "",
    surname: "",
    code: "",
}


export const auth = (state = authInitialState, action: AnyAction) => {
    
}