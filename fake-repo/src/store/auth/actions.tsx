import { AppDispatch } from "@store/storeConfig"
import { login as serviceLogin } from "@service/auth/auth"

export const ActionTypes = {
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGIN_FAIL: "LOGIN_FAIL",

}

export const login = (email: String, password: String) => {
    return async (dispatch: AppDispatch) => {
        const apiResponse = await serviceLogin(email, password)
        let payload
        if(true)
            dispatch({type: ActionTypes.LOGIN_SUCCESS, payload})
        else
            dispatch({type: ActionTypes.LOGIN_FAIL, payload})
    }
}